package com.devEducation;

import com.devEducation.socket.ServerSocket;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.devEducation.dao.MySqlDao.*;

public class Main {
    public static void main(String[] args) {
            ServerSocket socket = new ServerSocket();

            SocketChannel client = null;
            try {
                client = socket.initialize(9000);
            } catch (IOException e) {
                e.printStackTrace();
            }


            ByteBuffer byteBuffer = socket.getByteBuffer();

            StringBuilder str = new StringBuilder();

            String command = null;
            try {
                while (command == null) {
                    System.out.println("V While ");

//                str.append(1).append(",");
//                System.out.println(str);
//                Thread.sleep(1000);

                    //assert client != null;
                    // byteBuffer.flip();

                    client.read(byteBuffer);
                    byteBuffer.flip();
                    //System.out.println(byteBuffer.toString());
                    command = new String(byteBuffer.array(), StandardCharsets.UTF_8);
                    System.out.println(command);
                    byteBuffer.clear();

                }
            } catch (Exception e) {
                e.fillInStackTrace();
            }
            System.out.println("after while");
            int a = 0; // Начальное значение диапазона - "от"
            int b = 1000; // Конечное значение диапазона - "до"
            List<Integer> integers = new ArrayList<>();
            for (int i = 0; i < 100; i++) {
                integers.add(a + (int) (Math.random() * b));
            }
            insertNum(integers);
            List<Integer> integers1 = selectNum();

            String res = integers1.stream()
                    .map(Object::toString)
                    .collect(Collectors.joining(","));

//            try (FileWriter writer = new FileWriter("note.csv", false)) {
//                writer.write(res);
//                writer.flush();
//            } catch (IOException ex) {
//                System.out.println(ex.getMessage());
//            }


            byteBuffer.flip();
            try {
                byte[] byteArray = res.getBytes();
                client.write(ByteBuffer.wrap(byteArray));
            } catch (IOException e) {
                e.printStackTrace();
            }
            deleteAll();

    }

}
