package com.devEducation.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.devEducation.utils.UtilMySql.getDBConnection;

public class MySqlDao {

    public static void main(String[] args) {
        List<Integer> strings = new ArrayList<>();
        strings.add(1);
        strings.add(2);
        insertNum(strings);
        selectNum();
    }

    private static Connection c = getDBConnection();

    public static void insertNum(List<Integer> integers) {
        try {
            Connection c = getDBConnection();

            String STR = "INSERT INTO serverSoketChannel.numbers (nam) VALUES (?)";

            PreparedStatement statement = c.prepareStatement(
                    STR);

            for (Integer genre : integers) {
                statement.setInt(1, genre);
                //logger.info("Создана запись: " + genre);
                try {
                    statement.execute();
                } catch (SQLException ignored) {
                }

            }

            statement.clearParameters();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List selectNum() {

        List<Integer> strings = new ArrayList<>();
        try {
            Statement statement = c.createStatement();
            ResultSet set = statement.executeQuery(
                    "SELECT nam FROM serverSoketChannel.numbers"
            );

            while (set.next()) {
                strings.add(set.getInt(1));
            }
            statement.close();

//            for (Integer str : strings) {
//                System.out.println("Получена запись: " + str);
//            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return strings;
    }

    public static void deleteAll() {
        try {
            PreparedStatement statement =
                    c.prepareStatement(
                            "DELETE FROM serverSoketChannel.numbers "
                    );
            statement.execute();


            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
