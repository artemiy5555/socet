package com.devEducation.socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class ServerSocket {

    private ServerSocketChannel channel;

    private ByteBuffer byteBuffer = ByteBuffer.allocate(1024);


    public SocketChannel initialize(int port) throws IOException {
        channel = ServerSocketChannel.open();
        channel.socket().bind(new InetSocketAddress(port));

        return channel.accept();
    }

    public ByteBuffer getByteBuffer() {
        return this.byteBuffer;
    }

}
